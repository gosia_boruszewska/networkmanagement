package function.snmp;

import function.IntToHexConverter;

import java.util.ArrayList;

public class RequestId extends SNMPElement{
    private static String TYPE = new String("Integer");
    public RequestId(){
        super(TYPE);
    }
}
