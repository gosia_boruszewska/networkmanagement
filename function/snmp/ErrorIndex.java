package function.snmp;

import com.google.inject.Inject;

public class ErrorIndex extends SNMPElement{
    private static String TYPE = new String("Integer");

    @Inject
    public ErrorIndex() {
        super(TYPE);
    }
}
