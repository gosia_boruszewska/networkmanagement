package function.snmp;

import function.StringToHexConverter;

import java.util.ArrayList;

public class SNMPElement {
    private static String TYPE;
    private ArrayList<String> value;

    public SNMPElement(String TYPE){
        this.TYPE = TYPE;
    }

    public void setValue(String value) {
        StringToHexConverter stringToHexConverter = new StringToHexConverter();
        stringToHexConverter.setExpression(value);
        this.value = stringToHexConverter.convertStringToHex();
    }
    public void setValue(ArrayList<String> value) {
        this.value = value;
    }

    public static void setTYPE(String TYPE) {
        SNMPElement.TYPE = TYPE;
    }

    public String getTYPE() {
        return TYPE;
    }

    public ArrayList<String> getValue() {

        return value;
    }
}
