package com.journaldev.di.injector;

import com.google.inject.AbstractModule;
import function.snmp.SNMPBlock;

public class AppInjector extends AbstractModule {

    @Override
    protected void configure() {
        bind(SNMPBlock.class).to(SNMPBlock.class);
    }

}
