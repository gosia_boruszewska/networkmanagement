package function;

import java.util.ArrayList;

public class IntToHexConverter {
    public static String convertToHex(int expression) {
        return String.format("%04x", (expression)).substring(2);
    }

    public static ArrayList<String> convertToHexNumberBiggerThan255(int expression, ArrayList<String> stringArrayList){
        String hexNumber = String.format("%x", (expression));
        if(hexNumber.length() % 2 == 0) {
            for (int i = 0; i < hexNumber.length(); i = i + 2) {
                stringArrayList.add(String.valueOf(hexNumber.charAt(i)) + String.valueOf((hexNumber.charAt(i+1))));
            }
        }
        else {
            stringArrayList.add("0" + String.valueOf((hexNumber.charAt(0))));
            for (int i = 1; i < hexNumber.length(); i = i + 2) {
                stringArrayList.add(String.valueOf(hexNumber.charAt(i)) + String.valueOf((hexNumber.charAt(i+1))));
            }
        }
        return stringArrayList;
    }
}
