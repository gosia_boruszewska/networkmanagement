package function.snmp;

import function.StringToHexConverter;

import java.util.ArrayList;

public class CommunityString extends SNMPElement {
    private static String TYPE = new String("Octet String");

    public CommunityString() {
        super(TYPE);
    }
}
