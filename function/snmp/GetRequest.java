package function.snmp;

import java.util.ArrayList;

public class GetRequest extends SNMPElement{
    private static String TYPE = new String("Get Request");

    public GetRequest(){
        super(TYPE);
    }
}
