package function.snmp;

import com.google.inject.Inject;
import function.IntToHexConverter;

import java.util.ArrayList;

public class SNMPBlock {
    private String length;
    private ArrayList<String> value;
    private String type;
    private SNMPElement snmpElement;

    @Inject
    public SNMPBlock(){}

    private void prepareLength(){
        if (snmpElement.getTYPE().equals("Null")){
            length = "00";
            return;
        }
        int lengthOfData = snmpElement.getValue().size();
        length = IntToHexConverter.convertToHex(lengthOfData);
    }

    public void setType() {
        switch(snmpElement.getTYPE()){
            case "Sequence" : this.type = "30"; break;
            case "Integer" : this.type = "02"; break;
            case "Community String" : this.type = "04"; break;
            case "Get Request" : this.type= "A0"; break;
            case "Object Identifier" : this.type = "06"; break;
            case "Octet String" : this.type = "04"; break;
            case "Null" : this.type = "05"; break;
            default: break;
        }
    }

    public String getLength() {
        return length;
    }

    public ArrayList<String> getValue() {
        return value;
    }

    public String getType() {
        return type;
    }

    public void setValue() {
        this.value = snmpElement.getValue();
    }

    public void setSnmpElement(SNMPElement snmpElement) {
        this.snmpElement = snmpElement;
        prepareLength();
        setType();
        setValue();
    }
}
