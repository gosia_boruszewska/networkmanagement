package function;

import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class StringToObjectIdConverter implements convertToHex{

    private String expression;

    private void removeToSignificantNumber(ArrayList<Integer> expressionStringList){
        expressionStringList.remove(0);
        expressionStringList.remove(0);
    }

    private ArrayList<Integer> convertStringToIntegerArray(){
        String[] expressionArray = expression.split(Pattern.quote("."));
        ArrayList<Integer> expressionStringList = new ArrayList<Integer>();
        for(int i = 0 ; i < expressionArray.length; i++) {
            expressionStringList.add(Integer.parseInt(expressionArray[i]));
        }
        return expressionStringList;
    }

    private int toFirstExpression(int x, int y){
        return (40*x)+y;
    }

    private ArrayList<String> convertToHexTwoFirst(ArrayList<Integer> expressionStringList){
        ArrayList<String> expressionHexList = new ArrayList<>();
        int firstByte = toFirstExpression(expressionStringList.get(0),expressionStringList.get(1));
        expressionHexList.add(convertToHexSimply(firstByte));
        return expressionHexList;
    }

    private String convertToHexSimply(int sign){
        return IntToHexConverter.convertToHex(sign);
    }

    private ArrayList<String> convertToHexDifficult(int sign,ArrayList<String> expressionHexList ){
        int dividerOfTheSign = sign/128;
        int originalDividerOfTheSign = dividerOfTheSign;
        int finalRest = sign - dividerOfTheSign * 128;
        ArrayList<Integer> restArray = new ArrayList<Integer>();
        boolean isDividerMoreThan127 = dividerOfTheSign>127;
        while (isDividerMoreThan127) {
            restArray.add(sign - dividerOfTheSign * 128);
            sign = dividerOfTheSign;
            dividerOfTheSign = sign/128;
            isDividerMoreThan127 = dividerOfTheSign>127;
        }
        restArray.add(finalRest);
        Collections.reverse(restArray);
        expressionHexList.add(convertToHexSimply(originalDividerOfTheSign | (1 << 7) ));
        expressionHexList
                .addAll(restArray.stream()
                .map(this::convertToHexSimply)
                .collect(Collectors.toList()));
        return expressionHexList;
    }

    public ArrayList<String> convertStringToObjectId() {
        ArrayList<Integer> expressionIntList = convertStringToIntegerArray();
        ArrayList<String> expressionHexList = convertToHexTwoFirst(expressionIntList);
        removeToSignificantNumber(expressionIntList);
        for (Integer sign : expressionIntList){
            boolean signIsGreaterThan127 = (sign > 127);
            if(signIsGreaterThan127)
                expressionHexList = convertToHexDifficult(sign,expressionHexList);
            else
                expressionHexList.add(convertToHexSimply(sign));
        }
        return expressionHexList;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }
}
