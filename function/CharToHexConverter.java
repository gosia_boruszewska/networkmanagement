package function;

public class CharToHexConverter {
    public static String convertToHex(char expression){
        return String.format("%02x", (int) expression);
    }
}
