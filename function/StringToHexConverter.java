package function;


import java.util.ArrayList;
import java.util.stream.Collectors;

public class StringToHexConverter  {

    private String expression;

    private ArrayList<Character> convertStringToArray(String expression){
        ArrayList<Character> expressionList = new ArrayList<Character>();
        for (int i = 0; i < expression.length(); i++){
            expressionList.add(expression.charAt(i));
        }
        return expressionList;
    }

    public ArrayList<String> convertStringToHex() {
        ArrayList<String> expressionHexList = new ArrayList<>();
        if (this.expression.matches(".*[a-zA-Z]+.*")){
            ArrayList<Character> expressionCharList = convertStringToArray(this.expression);
            expressionHexList = expressionCharList.stream().map(CharToHexConverter::convertToHex).collect(Collectors.toCollection(ArrayList::new));
        }
        else {
            if ((Integer.parseInt(this.expression)) < 255) {
                if (Integer.parseInt(this.expression) > 127) expressionHexList.add("00");
                expressionHexList.add(IntToHexConverter.convertToHex(Integer.parseInt(this.expression)));
            }
            else {
                if (Integer.parseInt(this.expression) > 2048 && Integer.parseInt(this.expression) <= 4095) expressionHexList.add("00");
                expressionHexList = IntToHexConverter.convertToHexNumberBiggerThan255(Integer.parseInt(this.expression),expressionHexList);
            }
        }

        return expressionHexList;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getExpression(){
        return  this.expression;
    }
}
