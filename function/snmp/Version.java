package function.snmp;

import function.IntToHexConverter;
import function.StringToHexConverter;

import java.util.AbstractList;
import java.util.ArrayList;

public class Version extends SNMPElement {
    private static String TYPE = new String("Integer");
    public Version(){
        super(TYPE);
    }
}

