package function.snmp;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.journaldev.di.injector.AppInjector;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.logging.Logger;

public class SNMPMessage {
    private static String TYPE = new String("Sequence");
    private ArrayList<String> snmpMessage;
    Injector appInjector = Guice.createInjector();
    private Value value;
    private ObjectId objectId;
    private Varbind varbind;
    private  VarbindList varbindList;
    private RequestId requestId;
    private CommunityString communityString;
    private Error error;
    private ErrorIndex errorIndex;
    private Version version;
    private GetRequest getRequest;
    private AllMessage allMessage;

    private void addValue(){
        SNMPBlock snmpBlockValue = appInjector.getInstance(SNMPBlock.class);
        value = appInjector.getInstance(Value.class);
        value.setValue("0");
        snmpBlockValue.setSnmpElement(value);
        snmpMessage.add(snmpBlockValue.getLength());
        snmpMessage.add(snmpBlockValue.getType());
    }

    private void addObjectId(String ObjectID){
        SNMPBlock snmpBlockObjectId = appInjector.getInstance(SNMPBlock.class);
        objectId = appInjector.getInstance(ObjectId.class);
        objectId.setValue(ObjectID);
        snmpBlockObjectId.setSnmpElement(objectId);
        ArrayList<String> valueFromBlock = snmpBlockObjectId.getValue();
        Collections.reverse(valueFromBlock);
        snmpMessage.addAll(valueFromBlock);
        snmpMessage.add(snmpBlockObjectId.getLength());
        snmpMessage.add(snmpBlockObjectId.getType());
    }

    private void addVarbind(ArrayList<String> value){
        SNMPBlock snmpBlockVarbind = appInjector.getInstance(SNMPBlock.class);
        varbind = appInjector.getInstance(Varbind.class);
        varbind.setValue(value);
        snmpBlockVarbind.setSnmpElement(varbind);
        snmpMessage.add(snmpBlockVarbind.getLength());
        snmpMessage.add(snmpBlockVarbind.getType());
    }

    private void addVarbindList(ArrayList<String> value){
        SNMPBlock snmpBlockVarbindList = appInjector.getInstance(SNMPBlock.class);
        varbindList = appInjector.getInstance(VarbindList.class);
        varbindList.setValue(value);
        snmpBlockVarbindList.setSnmpElement(varbind);
        snmpMessage.add(snmpBlockVarbindList.getLength());
        snmpMessage.add(snmpBlockVarbindList.getType());
    }

    private void addErrorIndex(){
        SNMPBlock snmpBlockErrorIndex = appInjector.getInstance(SNMPBlock.class);
        errorIndex = appInjector.getInstance(ErrorIndex.class);
        errorIndex.setValue("0");
        snmpBlockErrorIndex.setSnmpElement(errorIndex);
        ArrayList<String> valueFromBlock = snmpBlockErrorIndex.getValue();
        Collections.reverse(valueFromBlock);
        snmpMessage.addAll(valueFromBlock);
        snmpMessage.add(snmpBlockErrorIndex.getLength());
        snmpMessage.add(snmpBlockErrorIndex.getType());
    }

    private void addError(){
        SNMPBlock snmpBlockError = appInjector.getInstance(SNMPBlock.class);
        error = appInjector.getInstance(Error.class);
        error.setValue("0");
        snmpBlockError.setSnmpElement(error);
        ArrayList<String> valueFromBlock = snmpBlockError.getValue();
        Collections.reverse(valueFromBlock);
        snmpMessage.addAll(valueFromBlock);
        snmpMessage.add(snmpBlockError.getLength());
        snmpMessage.add(snmpBlockError.getType());
    }

    private void addRequestId(String requestID){
        SNMPBlock snmpBlockRequestId = appInjector.getInstance(SNMPBlock.class);
        requestId = appInjector.getInstance(RequestId.class);
        requestId.setValue(requestID);
        snmpBlockRequestId.setSnmpElement(requestId);
        ArrayList<String> valueFromBlock = snmpBlockRequestId.getValue();
        Collections.reverse(valueFromBlock);
        snmpMessage.addAll(valueFromBlock);
        snmpMessage.add(snmpBlockRequestId.getLength());
        snmpMessage.add(snmpBlockRequestId.getType());
    }

    private void addGetRequest(ArrayList<String> value){
        SNMPBlock snmpBlockGetRequest = appInjector.getInstance(SNMPBlock.class);
        getRequest = appInjector.getInstance(GetRequest.class);
        requestId.setValue(value);
        snmpBlockGetRequest.setSnmpElement(requestId);
        snmpMessage.add(snmpBlockGetRequest.getLength());
        snmpMessage.add(snmpBlockGetRequest.getType());
    }

    private void addCommunityString(String value){
        SNMPBlock snmpBlockCommunityString = appInjector.getInstance(SNMPBlock.class);
        communityString = appInjector.getInstance(CommunityString.class);
        communityString.setValue(value);
        snmpBlockCommunityString.setSnmpElement(communityString);
        ArrayList<String> valueFromBlock = snmpBlockCommunityString.getValue();
        Collections.reverse(valueFromBlock);
        snmpMessage.addAll(valueFromBlock);
        snmpMessage.add(snmpBlockCommunityString.getLength());
        snmpMessage.add(snmpBlockCommunityString.getType());
    }


    private void addVersion(){
        SNMPBlock snmpBlockValue = appInjector.getInstance(SNMPBlock.class);
        version = appInjector.getInstance(Version.class);
        version.setValue("0");
        snmpBlockValue.setSnmpElement(value);
        ArrayList<String> valueFromBlock = snmpBlockValue.getValue();
        Collections.reverse(valueFromBlock);
        snmpMessage.addAll(valueFromBlock);
        snmpMessage.add(snmpBlockValue.getLength());
        snmpMessage.add(snmpBlockValue.getType());
    }

    private void addAllMessage(ArrayList<String> value) {
        SNMPBlock snmpBlockAllMessage = appInjector.getInstance(SNMPBlock.class);
        allMessage = appInjector.getInstance(AllMessage.class);
        allMessage.setValue(value);
        snmpBlockAllMessage.setSnmpElement(allMessage);
        snmpMessage.add(snmpBlockAllMessage.getLength());
        snmpMessage.add(snmpBlockAllMessage.getType());
    }

    public ArrayList<String> setSnmpMessage(String communityString, String requestId, String ObjectID){
        snmpMessage = new ArrayList<>();
        addValue();
        addObjectId(ObjectID);
        addVarbind(snmpMessage);
        addVarbindList(snmpMessage);
        addErrorIndex();
        addError();
        addRequestId(requestId);
        addGetRequest(snmpMessage);
        addCommunityString(communityString);
        addVersion();
        addAllMessage(snmpMessage);
        Collections.reverse(snmpMessage);
        return  snmpMessage;
    }
}
