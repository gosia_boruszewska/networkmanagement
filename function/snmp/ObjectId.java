package function.snmp;

import function.StringToObjectIdConverter;

import java.util.ArrayList;

public class ObjectId extends SNMPElement{
    private static String TYPE = new String("Object Identifier");
    private ArrayList<String> value;

    public ObjectId(){
        super(TYPE);
    }

    @Override
    public void setValue(String objectId) {
        StringToObjectIdConverter stringToObjectIdConverter = new StringToObjectIdConverter();
        stringToObjectIdConverter.setExpression(objectId);
        this.value = stringToObjectIdConverter.convertStringToObjectId();
    }

    @Override
    public ArrayList<String> getValue(){
        return this.value;
    }
}
