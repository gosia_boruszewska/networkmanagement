package function.snmp;

import com.google.inject.Inject;

public class Error extends SNMPElement{
    private static String TYPE = new String("Integer");

    @Inject
    public Error() {
        super(TYPE);
    }
}